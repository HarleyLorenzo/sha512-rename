#!/usr/bin/env bash
# Supply a file or a list of files and it will conver their filename into
# their SHA512 hash in hex while perserving their file extension

# show usage and exit with a particular error and exit code
function show_usage ()
{
    >&2 echo "Usage: $0 FILE..."
    exit 1
}

function show_version ()
{
    echo "Harley A.W. Lorenzo sha512-rename Version 1.1.0"
    echo "hl1998@protonmail.com"
    echo "GPG Key: 72BC 96FB D08A CFA8 BBD8 D1D6 F6EF 2390 4645 BA53"
    echo "https://gitlab.com/HarleyLorenzo/sha512-rename"
    exit 0
}

# check if there were no arguments provided or if show_usage was
if [ $# ==  0 ]; then
    show_usage
elif [ $1 == "show_usage" ]; then
    show_usage
elif [ $1 == "show_version" ]; then
    show_version
fi

# the main code
for file in "$@"; do 
    if [ -d "${file}" ]; then
        >&2 echo "${file} is a directory, ignoring..."
        continue
    elif [ ! -f "${file}" ]; then
    >&2 echo "File not found: ${file}"
        continue
    fi
    sha512sum "${file}" | # 
    while read -r sum filename; do
    dir=$(dirname "${file}")
    extension="${filename##*.}" # grab the shortest matching *.
    if [ "${filename}" == "${extension}" ]; then
        # filename and extension are the same if there was no . in filename
        # and in that case, don't use extension in renaming
        mv -v "${file}" "${dir}/${sum}"
    else
        mv -v "${file}" "${dir}/${sum}.${extension}"
    fi
    done
done
